﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace OtusEventLibrary
{
    public class DocumentsReceiver : IDisposable
    {
        public string TargetDirectory { get; }
        public uint TimeOut { get; }
        public bool KeepWatching { get { return _keepWatching; } }

        private bool _keepWatching;
        private FileSystemWatcher _watcher;
        private Timer _timer;
        private HashSet<string> _fileNameSet = new();

        public event Action DocumentsReady;
        public event Action TimedOut;

        public DocumentsReceiver(string targetDirectory, uint timeOut)
        {
            TargetDirectory = targetDirectory;
            TimeOut = timeOut;
            _keepWatching = true;
        }

        public void AddFileToWatch(string fileName)
        {
            _fileNameSet.Add(fileName);
        }

        public void Start()
        {
            _watcher = new FileSystemWatcher(TargetDirectory, "*.*");
            _watcher.NotifyFilter = NotifyFilters.FileName;
            _watcher.Created += CreatedHandler;
            _watcher.EnableRaisingEvents = true;

            _timer = new Timer(TimeOut * 1000);
            _timer.Elapsed += TimeOutHandler;
            _timer.Enabled = true;
        }

        private void CreatedHandler(object sender, FileSystemEventArgs e)
        {
            if (_fileNameSet.Contains(e.Name))
            {
                _fileNameSet.Remove(e.Name);
            }

            if (_fileNameSet.Count == 0)
            {
                Dispose();

                DocumentsReady?.Invoke();

                _keepWatching = false;
            }
        }

        private void TimeOutHandler(object sender, ElapsedEventArgs e)
        {
            Dispose();

            TimedOut?.Invoke();

            _keepWatching = false;
        }

        public void Dispose()
        {
            _watcher.Created -= CreatedHandler;
            _watcher.Dispose();

            _timer.Elapsed -= TimeOutHandler;
            _timer.Dispose();
        }
    }
}
